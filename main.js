let book = [
    {
        Name: "Halqa",
        Author: "Akrom Malik",
        publishing: "Asaxiy",
        cost: '65000',
        genre: 'Diniy',
        wrapper: "qattiq"
    },

    {
        Name: "Qiyomat",
        Author: "Imom G`azzoliy",
        publishing: "Hilol",
        cost: '84000',
        genre: 'Diniy',
        wrapper: "qattiq"
    },
    {
        Name: "Martin Iden",
        Author: "Jek London",
        publishing: "Asaxiy",
        cost: '29000',
        genre: 'Badiiy',
        wrapper: "yumshoq"
    },
    {
        Name: "Telba",
        Author: "Fyodor Dostoeviskiy",
        publishing: "Asaxiy",
        cost: '45000',
        genre: 'Badiiy',
        wrapper: "qattiq"
    },
    {
        Name: "Iskanja",
        Author: "Omina Shenliko`g`li",
        publishing: "Asaxiy",
        cost: '29000',
        genre: 'Badiiy',
        wrapper: "qattiq"
    },


];
let edited = -1;

function drawList() {
    document.getElementById('getBookList').innerHTML = '';
    for (let i = 0; i < book.length; i++) {
        document.getElementById('getBookList').innerHTML +=
            '<tr>' +
            '<td>' + (i + 1) + '</td>' +
            '<td>' + book[i].Name + '</td>' +
            '<td>' + book[i].Author + '</td>' +
            '<td>' + book[i].publishing + '</td>' +
            '<td>' + book[i].cost + '</td>' +
            '<td>' + book[i].genre + '</td>' +
            '<td>' + book[i].wrapper + '</td>' +
            '<td><button onclick="editbook(' + i + ')" type="button" class="btn btn-warning btn-sm"><i class="fas fa-edit"></i></button></td>' +
            '<td><button onclick="deletebook(' + i + ')" type="button" class="btn btn-danger btn-sm"><i class="fas fa-trash-alt"></i></button></td>' +
            '</tr>'
    }
}
drawList();

function addBook() {
    let Name = document.getElementById('Name').value;
    let Author = document.getElementById('Author').value;
    let publishing = document.getElementById('publishing').value;
    let cost = document.getElementById('cost').value;
    let genre = document.getElementById('genre').value;
    let wrapper = document.getElementById('wrapper').value;


    if (Name.trim().length > 0 && Author.trim().length > 0 &&
        publishing.trim().length > 0 && cost.trim().length > 0 && genre.trim().length > 0) {
        let newbook = {
            Name: Name,
            Author: Author,
            publishing: publishing,
            cost: cost,
            genre: genre,
            wrapper: wrapper
        };
        if (edited >= 0) {
            book[edited] = newbook;
            edited = -1
        }
        else {
            book.push(newbook);
        }

        drawList();
        document.getElementById('Name').value = '';
        document.getElementById('Author').value = '';
        document.getElementById('publishing').value = '';
        document.getElementById('cost').value = '';
        document.getElementById('genre').value = '';
        document.getElementById('wrapper').value = ''

    }
    else {
        alert("Formani to'liq to'ldiring!")
    }
}

function deletebook(index) {
    book.splice(index, 1);
    drawList()
}

function editbook(index) {
    document.getElementById('Name').value = book[index].Name;
    document.getElementById('Author').value = book[index].Author;
    document.getElementById('publishing').value = book[index].publishing;
    document.getElementById('cost').value = book[index].cost;
    document.getElementById('genre').value = book[index].genre;
    document.getElementById('wrapper').value = book[index].wrapper;
    edited = index;
}
